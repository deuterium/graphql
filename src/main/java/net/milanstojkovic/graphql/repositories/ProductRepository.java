package net.milanstojkovic.graphql.repositories;

import net.milanstojkovic.graphql.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findAllByVendor(String vendor);
}
