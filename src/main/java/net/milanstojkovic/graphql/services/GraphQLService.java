package net.milanstojkovic.graphql.services;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import net.milanstojkovic.graphql.models.Product;
import net.milanstojkovic.graphql.repositories.ProductRepository;
import net.milanstojkovic.graphql.services.fetcher.AllProductsDataFetcher;
import net.milanstojkovic.graphql.services.fetcher.ProductDataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

@Service
public class GraphQLService {

    @Autowired
    private ProductRepository productRepository;
    @Value("classpath:product.graphql")
    private Resource resource;
    private GraphQL graphQL;
    @Autowired
    private AllProductsDataFetcher allProductsDataFetcher;
    @Autowired
    private ProductDataFetcher productDataFetcher;

    // load schema at application start up
    @PostConstruct
    private void loadSchema() throws IOException {

        //Load Products into the Product Repository
//        loadDataIntoDB();
        // get the schema
        File schemaFile = resource.getFile();
        // parse schema
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemaFile);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private void loadDataIntoDB() {
        Stream.of(
                new Product("64-17-5", "Ethanol", "CH3CH2OH", "46.07", new String[]{"1L", "5L", "250ML", "500ML"},"Sigma"),
                new Product("67-66-3", "Chloroform", "CHCl3", "119.38", new String[]{"1L", "2.5L"},"MERCK"),
                new Product("60-00-4", "Ethylenediaminetetraacetic acid", "(HO2CCH2)2NCH2CH2N(CH2CO2H)2", "292.24", new String[]{"1000", "10000"},"MERCK")
        ).forEach(product -> {
            productRepository.save(product);
        });
    }

    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("allProducts", allProductsDataFetcher)
                        .dataFetcher("product", productDataFetcher))
                .build();
    }


    public GraphQL getGraphQL() {
        return graphQL;
    }

}
