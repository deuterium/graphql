package net.milanstojkovic.graphql.services.fetcher;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import net.milanstojkovic.graphql.models.Product;
import net.milanstojkovic.graphql.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllProductsDataFetcher implements DataFetcher<List<Product>> {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> get(DataFetchingEnvironment dataFetchingEnvironment) {
        return productRepository.findAll();
    }
}
