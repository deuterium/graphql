package net.milanstojkovic.graphql.services.fetcher;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import net.milanstojkovic.graphql.models.Product;
import net.milanstojkovic.graphql.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductDataFetcher implements DataFetcher<Product> {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product get(DataFetchingEnvironment dataFetchingEnvironment) {
        String casNumber = dataFetchingEnvironment.getArgument("casNumber");
        return productRepository.findOne(casNumber);
    }
}
