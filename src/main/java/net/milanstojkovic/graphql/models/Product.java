package net.milanstojkovic.graphql.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Table
@Entity
public class Product {
    @Id
    private String casNumber;
    private String name;
    private String formula;
    private String molWeight;
    private String[] mass;
    private String vendor;

    public Product() {
    }

    public Product(String casNumber, String name, String formula, String molWeight, String[] mass, String vendor) {
        this.casNumber = casNumber;
        this.name = name;
        this.formula = formula;
        this.molWeight = molWeight;
        this.mass = mass;
        this.vendor = vendor;
    }

    public String getCasNumber() {
        return casNumber;
    }

    public void setCasNumber(String casNumber) {
        this.casNumber = casNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getMolWeight() {
        return molWeight;
    }

    public void setMolWeight(String molWeight) {
        this.molWeight = molWeight;
    }

    public String[] getMass() {
        return mass;
    }

    public void setMass(String[] mass) {
        this.mass = mass;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
}
