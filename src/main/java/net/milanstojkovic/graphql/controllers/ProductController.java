package net.milanstojkovic.graphql.controllers;

import graphql.ExecutionResult;
import net.milanstojkovic.graphql.models.Product;
import net.milanstojkovic.graphql.repositories.ProductRepository;
import net.milanstojkovic.graphql.services.GraphQLService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private GraphQLService graphQLService;

    @Autowired
    private ProductRepository productRepository;

    @PostMapping
    public ResponseEntity<Object> allProducts(@RequestBody String query){
        ExecutionResult result = graphQLService.getGraphQL().execute(query);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{vendor}")
    public ResponseEntity<Object> allProductsByVendor(@PathVariable String vendor){
        List<Product> productList = productRepository.findAllByVendor(vendor);
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @GetMapping("/csv")
    public ResponseEntity<InputStreamResource> downloadCSV() throws IOException {

//        String csvFileName = "users.csv";
//
////        response.setContentType("text/csv");
//
//        // creates mock data
//        String headerKey = "Content-Disposition";
//        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
////        response.setHeader(headerKey, headerValue);
//
//        File file = new File(csvFileName);
//
//        String[] HEADERS = {"Name", "Surname" ,"Username",""};
//
//        FileWriter out = new FileWriter(csvFileName);
//
////        try(PrintWriter pw = new PrintWriter(new BufferedWriter( new FileWriter(file)))){
//        try(CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(HEADERS))){
//
//            List<Product> products = productRepository.findAll();
//            products.forEach(p -> {
//                try {
//                    printer.printRecord(p.getName());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            });
//
////            OutputStream outputStream = response.getOutputStream();
//
//        }catch (Exception e){
//
//        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/csv");
//        String csvFileName = "users.csv";
//        String csvFileName = "D:\\MilanTestProjects\\graphql\\src\\main\\resources\\product.graphql";
        String csvFileName = "milan.csv";
        File file = new File(csvFileName);
        FileWriter out = new FileWriter(file);
        String[] HEADERS = {"Name", "Surname" ,"Username",""};
        try(CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(HEADERS))) {

            List<Product> products = productRepository.findAll();
            products.forEach(p -> {
                try {
                    printer.printRecord(p.getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }catch (Exception e){

        }

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
//                .headers(headers)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                .contentLength(file.length())
//                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .contentType(MediaType.parseMediaType("text/csv"))
                .body(resource);
    }


    @GetMapping("/csv2")
    public void downloadCSV2(HttpServletResponse response)throws IOException{

        response.setContentType("text/csv");
        String reportName = "CSV_Report_Name.csv";
        response.setHeader("Content-disposition", "attachment;filename="+reportName);

        ArrayList<String> rows = new ArrayList<>();
        rows.add("Name,Result");
        rows.add("\n");

        for (int i = 0; i < 10; i++) {
            rows.add("Java Honk,Success");
            rows.add("\n");
        }

        Iterator<String> iter = rows.iterator();
        while (iter.hasNext()) {
            String outputString = iter.next();
            response.getOutputStream().print(outputString);
        }

        response.getOutputStream().flush();
    }


}
